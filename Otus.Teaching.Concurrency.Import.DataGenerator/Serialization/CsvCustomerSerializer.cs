﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Serialization
{
    /// <summary>
    /// Класс сериализации данных.
    /// </summary>
    public static class CsvCustomerSerializer
    {
        /// <summary>
        /// Производит сериализация данных IEnumerable<Customer> в csv файл. 
        /// </summary>
        /// <param name="stream">Поток для записи данных.</param>
        /// <param name="items">Исходный список данных.</param>
        public static void Serialise(Stream stream, IEnumerable<Customer> items)
        {
            if (!items.Any())
            {
                throw new ArgumentNullException(nameof(items));
            }

            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            foreach (var item in items)
            {
                var props = item.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                var strData = new StringBuilder();

                var isFirst = true;

                foreach (var prop in props)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                    }
                    else
                    {
                        strData.Append(",");
                    }

                    strData.Append(prop.GetValue(item));
                }

                strData.Append('\n');

                Write(stream, strData.ToString());
            }
        }

        private static void Write(Stream stream, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            
            stream.Write(info, 0, info.Length);
        }
    }
}
