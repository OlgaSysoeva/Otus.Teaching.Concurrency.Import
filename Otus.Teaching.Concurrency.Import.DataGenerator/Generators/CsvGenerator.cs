﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Serialization;
using Otus.Teaching.Concurrency.Import.Handler.Data;

using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    /// <summary>
    /// Класс генерации csv файла.
    /// </summary>
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _filePath;

        private readonly int _dataCount;

        public CsvGenerator(string filePath, int dataCount)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException(nameof(filePath));
            }

            if (dataCount <= 0)
            {
                throw new ArgumentException(nameof(dataCount));
            }

            _filePath = filePath;

            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            using var stream = File.Create(_filePath);

            CsvCustomerSerializer.Serialise(stream, customers);
        }
    }
}
