﻿using Microsoft.Extensions.Configuration;

using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static int _attemptsCount;

        private static readonly string _csvFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        
        private static int _dataCount;
        
        private static string _generatorPath;

        private static string _mongoConnection;


        private static bool _runByProcess;

        private static int _threadsCount;


        static async Task Main()
        {
            GetConfiguration();

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}.");

            if (_runByProcess == true)
            {
                // Запуск программы-генератора в новом процессе.
                StartNewProcess();
            }
            else
            {
                // Запуск генератора через вызов метода.
                GenerateCustomersDataFile();
            }

            // Количество попыток.
            int repeatsNum = 1;

            // Загрузка данных из файла в базу.
            while (repeatsNum <= _attemptsCount)
            {
                try
                {
                    await DataLoaderAsync();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception: {ex.Message}");

                    if (++repeatsNum <= _attemptsCount)
                    {
                        Console.WriteLine("Try again...");
                    }
                }
            }
        }

        /// <summary>
        /// Производит запись данных из файла в базу.
        /// </summary>
        /// <returns></returns>
        static async Task DataLoaderAsync()
        {
            IDataParser<List<Customer>> threadParser = new ThreadParse(_csvFilePath, _threadsCount);

            Console.WriteLine("Start the parser...");

            var leadTime = threadParser.Parse();

            Console.WriteLine($"Thread count {_threadsCount}.");

            Console.WriteLine("Type of work: {0}, elapsed time: {1}", leadTime.ThreadType, leadTime.Ellapsed);

            IDataParser<List<Customer>> threadPoolParser = new ThreadPoolParse(_csvFilePath, _threadsCount);

            leadTime = threadPoolParser.Parse();

            Console.WriteLine("Type of work: {0}, elapsed time: {1}", leadTime.ThreadType, leadTime.Ellapsed);

            Console.WriteLine("The end of the parser.");

            Console.WriteLine("Start saving to database...");

            IDataLoader Loader = new DataLoader(threadParser.ListCustomers.ToList(), _mongoConnection);

            await Loader.LoadDataAsync();

            Console.WriteLine("End of saving to database.");
        }

        /// <summary>
        /// Запускает генерацию файла.
        /// </summary>
        static void GenerateCustomersDataFile()
        {
            IDataGenerator csvGenerator = new CsvGenerator(_csvFilePath, _dataCount);

            Console.WriteLine("Start generator ...");

            csvGenerator.Generate();

            Console.WriteLine("End of generation.");
        }

        /// <summary>
        /// Получает параметры из конфигурационного файла.
        /// </summary>
        static void GetConfiguration()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var generatorOptions = configuration.GetSection("generatorOptions");
            
            var loadOptions = configuration.GetSection("loadOptions");

            _generatorPath = generatorOptions["generatorPath"];

            _mongoConnection = configuration.GetConnectionString("MongoConnection");

            if (string.IsNullOrEmpty(_generatorPath))
            {
                throw new ArgumentNullException(nameof(_generatorPath));
            }

            if (string.IsNullOrEmpty(_mongoConnection))
            {
                throw new ArgumentNullException(nameof(_mongoConnection));
            }

            if (!int.TryParse(generatorOptions["dataCount"], out _dataCount))
            {
                throw new ArgumentException(nameof(_dataCount));
            }

            if (!bool.TryParse(generatorOptions["runByProcess"], out _runByProcess))
            {
                throw new ArgumentException(nameof(_runByProcess));
            }

            if (!int.TryParse(loadOptions["threadsCount"], out _threadsCount))
            {
                throw new ArgumentException(nameof(_threadsCount));
            }

            if (!int.TryParse(loadOptions["attemptsCount"], out _attemptsCount))
            {
                throw new ArgumentException(nameof(_attemptsCount));
            }

            Console.WriteLine("Configuration data received.");
        }

        /// <summary>
        /// Запускает генерацию данных в новом процессе.
        /// </summary>
        static void StartNewProcess()
        {
            var process = Process.Start(_generatorPath, $"{_csvFilePath} {_dataCount}");

            Console.WriteLine($"Generator started with process Id {process.Id}.");

            process.WaitForExit();
        }
    }
}