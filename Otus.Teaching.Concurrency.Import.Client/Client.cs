﻿using Microsoft.Extensions.Configuration;

using System;
using System.Net.Http;

namespace Otus.Teaching.Concurrency.Import.Client
{
    /// <summary>
    /// Класс настроек подключения к серверу.
    /// </summary>
    public class Client
    {
        public HttpClient ClientHttp { get; }

        public Client()
        {
            var uriServer = GetParamConfig("httpAddressServer");

            ClientHttp = new HttpClient
            {
                BaseAddress = new Uri(uriServer)
            };
        }

        /// <summary>
        /// Считывает параметры из конфигурации.
        /// </summary>
        /// <param name="nameParam">Строковое наименование параметра.</param>
        /// <returns>Возвращает значение параметра.</returns>
        string GetParamConfig(string nameParam)
        {
            // подключение файла конфигурации
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            var param = configuration[nameParam];

            return param;
        }
    }
}
