﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    class Program
    {
        static async Task Main()
        {
            while (true)
            {
                CustomerOperations custOper = new CustomerOperations(new Client());

                custOper.GenerateCustomer();

                await custOper.AddAsync();

                await custOper.FindCustomer();

                Console.WriteLine();
            }
        }
    }
}
