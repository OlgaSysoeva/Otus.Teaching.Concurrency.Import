﻿using Newtonsoft.Json;

using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    /// <summary>
    /// Класс клиентских операций.
    /// </summary>
    public class CustomerOperations
    {
        Customer _customer;
        HttpClient _client;

        public CustomerOperations(Client client)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            _client = client.ClientHttp;
        }

        /// <summary>
        /// Вызывает удаленный метод добавления клиента в базу.
        /// </summary>
        /// <returns></returns>
        public async Task AddAsync()
        {
            Console.WriteLine("Запись в базу...");

            var content = new StringContent(
                JsonConvert.SerializeObject(_customer),
                Encoding.UTF8,
                "application/json");

            var response = await _client.PostAsync($"customer", content);

            await GetResult(response, "Добавлен клиент");
        }

        /// <summary>
        /// Метод поиска клиента.
        /// </summary>
        /// <returns></returns>
        public async Task FindCustomer()
        {
            Console.Write("Введите идентификатор клиента для поиска: ");

            if (int.TryParse(Console.ReadLine(), out int customerId))
            {
                await GetAsync(customerId);
            }
        }

        /// <summary>
        /// Генерирует нового клиента.
        /// </summary>
        public void GenerateCustomer()
        {
            Console.WriteLine("Генерация пользователя...");

            _customer = RandomCustomerGenerator.Generate(1).Single();
            _customer.Id = new Random().Next(10000);
        }

        /// <summary>
        /// Вызывает удаленый метод полуения клиента по Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        async Task GetAsync(int id)
        {
            var response = await _client.GetAsync($"customer/{id}");

            await GetResult(response, "Найденный клиент");
        }

        /// <summary>
        /// Выводит информацию по ответу от сервера.
        /// </summary>
        /// <param name="response">Ответ сервера.</param>
        /// <param name="text">Сообщение в текстовом преставлении.</param>
        /// <returns></returns>
        async Task GetResult(HttpResponseMessage response, string text)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }

            var result = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine($"{text}: {result}");
                return;
            }

            Console.WriteLine($"code {response.StatusCode}");
        }
    }
}
