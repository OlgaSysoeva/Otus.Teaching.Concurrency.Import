﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    /// <summary>
    /// Реализует интерфейс IDataLoader.
    /// </summary>
    public class DataLoader : IDataLoader
    {
        /// <summary>
        /// Список клиентов.
        /// </summary>
        private List<Customer> _customers;

        /// <summary>
        /// Строка подключения к базе mongo.
        /// </summary>
        private string _mongoConnection;

        public DataLoader(List<Customer> customers, string mongoConnection)
        {
            if (customers.Count == 0)
            {
                throw new ArgumentException(nameof(customers));
            }

            if (string.IsNullOrEmpty(mongoConnection))
            {
                throw new ArgumentException(nameof(mongoConnection));
            }

            _customers = customers;

            _mongoConnection = mongoConnection;
        }

        public async Task LoadDataAsync()
        {
            ICustomerRepository rep = new CustomerRepository(_mongoConnection);

             await rep.AddCustomersAsync(_customers);
        }
    }
}
