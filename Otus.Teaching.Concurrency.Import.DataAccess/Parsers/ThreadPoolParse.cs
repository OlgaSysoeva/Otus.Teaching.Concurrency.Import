﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    /// <summary>
    /// Реализует интефейс IDataParser с типом <List<Customer>>.
    /// </summary>
    public class ThreadPoolParse : IDataParser<List<Customer>>
    {
        /// <summary>
        /// Путь к файлу csv,
        /// </summary>
        private string _csvFilePath;

        public BlockingCollection<Customer> ListCustomers { get; set; }

        /// <summary>
        /// Количество потоков.
        /// </summary>
        private int _threadCount;

        public ThreadPoolParse(
            string csvFilePath,
            int threadCount)
        {
            _csvFilePath = csvFilePath;

            _threadCount = threadCount;

            ListCustomers = new BlockingCollection<Customer>();
        }

        public ThreadInfo Parse()
        {
            var lines = File.ReadAllLines(_csvFilePath);

            var resetEvent = new ManualResetEvent(false);

            var stopWatch = new Stopwatch();

            stopWatch.Start();

            ThreadPool.QueueUserWorkItem(x =>
            {
                Deserialiser.Deserialise(lines.ToList(), ListCustomers);

                resetEvent.Set();
            });

            resetEvent.WaitOne();

            stopWatch.Stop();

            var leadTime = new ThreadInfo
            {
                ThreadType =  ThreadTypeEnum.ThreadPool.ToString(),
                Ellapsed = stopWatch.ElapsedMilliseconds
            };

            return leadTime;
        }
    }
}
