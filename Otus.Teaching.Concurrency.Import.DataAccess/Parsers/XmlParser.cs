﻿using System.Collections.Concurrent;
using System.Collections.Generic;

using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public BlockingCollection<Customer> ListCustomers { get; set; }

        ThreadInfo IDataParser<List<Customer>>.Parse()
        {
            return null;
        }
    }
}