﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public static class Deserialiser
    {
        /// <summary>
        /// Преобразовывает строковые данные в модель.
        /// </summary>
        /// <param name="listDatas">Список строковых значений.</param>
        public static void Deserialise(List<string> listDatas, BlockingCollection<Customer> listCustomer)
        {
            if (!listDatas.Any())
            {
                throw new ArgumentNullException();
            }

            foreach (var data in listDatas)
            {
                var strMas = data.Split(',');

                Customer clsData = new Customer();

                var props = clsData.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                if (strMas.Count() != props.Length)
                {
                    throw new TargetParameterCountException(strMas.ToString());
                }

                var numMas = 0;

                foreach (var prop in props)
                {
                    var value = ConvertType(strMas[numMas], prop.PropertyType);

                    prop.SetValue(clsData, value);

                    numMas++;
                }

                listCustomer.Add(clsData);
            }
        }

        /// <summary>
        /// Производит преобразование типов.
        /// </summary>
        /// <param name="value">Значение для преобразования типа.</param>
        /// <param name="type">Тип к которому необхоимо преобразовать.</param>
        /// <returns>Возвращает преобразованное значение.</returns>
        private static dynamic ConvertType(string value, Type type)
        {
            if (type == typeof(int))
            {
                return int.Parse(value);
            }

            return value;
        }
    }
}
