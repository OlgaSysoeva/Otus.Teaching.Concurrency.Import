﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    /// <summary>
    /// Реализует интефейс IDataParser с типом <List<Customer>>.
    /// </summary>
    public class ThreadParse : IDataParser<List<Customer>>
    {
        /// <summary>
        /// Путь к файлу csv,
        /// </summary>
        private string _csvFilePath;

        public BlockingCollection<Customer> ListCustomers { get; set; }

        /// <summary>
        /// Количество потоков.
        /// </summary>
        private int _threadCount;

        public ThreadParse(
            string csvFilePath,
            int threadCount)
        {
            _csvFilePath = csvFilePath;

            _threadCount = threadCount;

            ListCustomers = new BlockingCollection<Customer>();
        }

        public ThreadInfo Parse()
        {
            var lines = File.ReadAllLines(_csvFilePath);

            var events = new List<ManualResetEvent>();

            var stopWatch = new Stopwatch();

            stopWatch.Start();

            // Рассчитываем количество строк для маленьких массивов.
            var arrayCount = Math.Ceiling((double)lines.Length / _threadCount);

            // Получаем список маленьких массивов.
            var arraySplit = lines.Split(Convert.ToInt32(arrayCount));

            foreach (var item in arraySplit)
            {
                var resetEvent = new ManualResetEvent(false);

                var thread = new Thread(() =>
                {
                    Deserialiser.Deserialise(item.ToList(), ListCustomers);

                    resetEvent.Set();
                });

                events.Add(resetEvent);

                thread.Start();
            }

            WaitHandle.WaitAll(events.ToArray());

            stopWatch.Stop();

            var leadTimes = new ThreadInfo
            {
                ThreadType =  ThreadTypeEnum.Thread.ToString(),
                Ellapsed = stopWatch.ElapsedMilliseconds
            };

            return leadTimes;
        }
    }
}
