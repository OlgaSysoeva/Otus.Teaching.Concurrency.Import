﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class CustomerRepository
        : ICustomerRepository
    {
        /// <summary>
        /// Объект базы данных MongoDb.
        /// </summary>
        private IMongoDatabase _dataBase;

        public CustomerRepository(string mongoConnection)
        {
            if (string.IsNullOrEmpty(mongoConnection))
            {
                throw new ArgumentException(nameof(mongoConnection));
            }

            _dataBase = new MongoClient(mongoConnection)
                .GetDatabase("mongo");
        }

        public async Task AddCustomersAsync(IEnumerable<Customer> customers)
        {
            if (customers == null)
            {
                throw new ArgumentNullException(nameof(customers));
            }

            if (customers.Count() == 0)
            {
                throw new ArgumentException(nameof(customers));
            }

            var collection = _dataBase.GetCollection<BsonDocument>("customer");

            var customersBSon = customers.Select(x => x.ToBsonDocument());

            await collection.InsertManyAsync(customersBSon);
        }

        public async Task AddCustomersAsync(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
            }

            var collection = _dataBase.GetCollection<BsonDocument>("customer");

            var customerBSon = customer.ToBsonDocument();

            await collection.InsertOneAsync(customerBSon);
        }

        public async Task<Customer> GetCustomersAsync(int customerId)
        {
            var collection = _dataBase.GetCollection<BsonDocument>("customer");

            var filter = new BsonDocument("_id", customerId);
            var customerBson = await collection.Find(filter).FirstOrDefaultAsync();

            if (customerBson == null)
            {
                return null;
            }

            var customer = BsonSerializer.Deserialize<Customer>(customerBson);
            return customer;
        }
    }
}