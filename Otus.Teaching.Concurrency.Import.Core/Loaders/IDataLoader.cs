﻿using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    /// <summary>
    /// Интерфейс загрузни данных в базу.
    /// </summary>
    public interface IDataLoader
    {
        /// <summary>
        /// Загружает данные в базу.
        /// </summary>
        /// <returns></returns>
        Task LoadDataAsync();
    }
}