﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum ThreadTypeEnum
    {
        Thread = 1,

        ThreadPool = 2
    }
}
