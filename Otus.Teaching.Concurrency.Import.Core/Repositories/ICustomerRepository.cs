﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    /// <summary>
    /// Интерфейс репозитория.
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Добавляет клиентов в базу.
        /// </summary>
        /// <param name="customers">Список клиентов.</param>
        /// <returns></returns>
        Task AddCustomersAsync(IEnumerable<Customer> customers);

        /// <summary>
        /// Добавляет клиента в базу.
        /// </summary>
        /// <param name="customer">Данные клиента.</param>
        /// <returns></returns>
        Task AddCustomersAsync(Customer customer);

        /// <summary>
        /// Получает клиента с заданным идентификатором.
        /// </summary>
        /// <param name="customerId">Идентификатор клиента.</param>
        /// <returns>Возвращает данные клиента.</returns>
        Task<Customer> GetCustomersAsync(int customerId);
    }
}