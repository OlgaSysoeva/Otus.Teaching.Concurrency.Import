﻿namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    /// <summary>
    /// Интерфейс генерации данных.
    /// </summary>
    public interface IDataGenerator
    {
        /// <summary>
        /// Генерирует данные.
        /// </summary>
        void Generate();
    }
}