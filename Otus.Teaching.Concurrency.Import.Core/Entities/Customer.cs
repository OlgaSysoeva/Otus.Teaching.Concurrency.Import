﻿namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    /// <summary>
    /// Модель клиента.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Иентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Полное имя клиента.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Электронная почта клиента.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона клиента.
        /// </summary>
        public string Phone { get; set; }
    }
}