﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{
    /// <summary>
    /// Интерфейс считывания файла.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataParser<T>
    {
        /// <summary>
        /// Список клиентов.
        /// </summary>
        BlockingCollection<Customer> ListCustomers { get; set; }

        /// <summary>
        /// Парсит данные из файла в список объектов.
        /// </summary>
        /// <returns>Вовращает тип используемого потока и затраченное время.</returns>
        ThreadInfo Parse();
    }
}