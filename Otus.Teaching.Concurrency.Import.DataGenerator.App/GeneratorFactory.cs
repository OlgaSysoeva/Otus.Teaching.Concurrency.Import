﻿using Otus.Teaching.Concurrency.Import.Handler.Data;

using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.CsvGenerator
{
    /// <summary>
    /// Класс генерации файла csv.
    /// </summary>
    public static class GeneratorFactory
    {
        /// <summary>
        /// Получает объект класса генератора файла.
        /// </summary>
        /// <param name="fileName">Путь с именем файла для генерации.</param>
        /// <param name="dataCount">Количество генерируемых записей</param>
        /// <returns>Объект интерфейса генератора.</returns>
        public static IDataGenerator GetGenerator(string filePath, int dataCount)
        {
            return new CsvDataGenerator(filePath, dataCount);
        }
    }
}