﻿using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.CsvGenerator
{
    class Program
    {
        private static string _dataFilePath;

        private static int _dataCount; 
        
        static void Main(string[] args)
        {
            TryValidateAndParseArgs(args);
            
            Console.WriteLine("Generating csv data...");

            var generator = GeneratorFactory.GetGenerator(_dataFilePath, _dataCount);
            
            generator.Generate();
            
            Console.WriteLine($"Generated csv data in {_dataFilePath}.");
        }

        /// <summary>
        /// Получает и проверяет входные параметры.
        /// </summary>
        /// <param name="args">Входные параметры командной строки.</param>
        private static void TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFilePath = args[0];

                if (string.IsNullOrEmpty(_dataFilePath))
                {
                    throw new ArgumentNullException(nameof(_dataFilePath));
                }

                if (!int.TryParse(args[1], out _dataCount))
                {
                    throw new ArgumentException(nameof(_dataCount));
                }
            }
            else
            {
                throw new ArgumentException("No input arguments.");
            }
        }
    }
}