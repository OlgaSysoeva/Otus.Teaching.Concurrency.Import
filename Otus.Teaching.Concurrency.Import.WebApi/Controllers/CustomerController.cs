﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    /// <summary>
    /// Контроллер для взаимодействия с таблицей клиентов.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
       
        private readonly ILogger<CustomerController> _logger;
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(
            ILogger<CustomerController> logger,
            ICustomerRepository customerRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Метод получения клиента по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор клиента.</param>
        /// <returns>Возвращает статус код.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var customer = await _customerRepository.GetCustomersAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        /// <summary>
        /// Добавляет коиента в базу.
        /// </summary>
        /// <param name="customer">Данные клиента</param>
        /// <returns>Возвращает статус код.</returns>
        [HttpPost]
        public async Task<IActionResult> Add(Customer customer)
        {
            if (customer == null)
            {
                return BadRequest(customer);
            }

            var customerFind = await _customerRepository.GetCustomersAsync(customer.Id);

            if (customerFind == null)
            {
                await _customerRepository.AddCustomersAsync(customer);
                return Ok(customer);
            }

            return Conflict();
        }
    }
}
